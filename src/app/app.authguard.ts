import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router
} from '@angular/router';
import { KeycloakAuthGuard, KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';
 
@Injectable({
  providedIn: 'root',
})
export class AuthGuard extends KeycloakAuthGuard {
  constructor(
    protected readonly router: Router,
    protected readonly keycloak: KeycloakService
  ) {
    super(router, keycloak);
  }
 
  public async isAccessAllowed(route: ActivatedRouteSnapshot): Promise<boolean> {
    
    return new Promise((resolve, reject) => {
      if (!this.authenticated) {
        this.keycloakAngular.login({redirectUri: window.location.origin + environment.webAdress})
          .catch(e => console.error(e));
        return reject(false);
      }
      

      const requiredRoles: string[] = route.data.roles;
      if (!requiredRoles || requiredRoles.length === 0) {
        return resolve(true);
      } else {
        if (!this.roles || this.roles.length === 0) {
          resolve(false);
        }
        resolve(requiredRoles.every(role => this.roles.indexOf(role) > -1));
      }
    });
  }
}
