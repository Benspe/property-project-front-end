import { Component, OnInit } from '@angular/core';
import { AccountRepositoryService } from 'src/app/services/AccountService/account-repository.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public account: any = [];

  constructor(private accountAPI: AccountRepositoryService) { }

  ngOnInit(): void {
    this.loadPropertiesFromAPI()
  }
  
  async loadPropertiesFromAPI(){
    await this.tryToCreateProfile();
    try
    {
      this.account = await this.accountAPI.getAccountInfo();
    }catch(e)
    {
      console.error(e.error);
    }
  }
  async tryToCreateProfile(){
    try
    {
      await this.accountAPI.createAccount();
    }catch(e)
    {
      console.error(e.error);
    }
    
  }
  updateProfile(){
    console.log("Not yet implemented");
  }
}
