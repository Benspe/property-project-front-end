import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router, private keycloakService: KeycloakService) { }

  ngOnInit() {}

  home(){
    this.router.navigateByUrl('home');
  }

  catalogue(){
    this.router.navigateByUrl('catalogue');
  }

  profile(){
    this.router.navigateByUrl('profile');
  }
  async logout() {
    await this.keycloakService.logout(window.location.origin + environment.webAdress);
    console.log(window.location.origin + environment.webAdress)
    this.router.navigateByUrl('');
    location.reload();
  }

}
