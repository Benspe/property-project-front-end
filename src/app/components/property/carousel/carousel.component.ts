import { Component, Input, OnInit } from '@angular/core';

import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})
export class CarouselComponent implements OnInit {

  @Input() images;
  @Input() blur;
  constructor(config: NgbCarouselConfig) {
    config.interval = 6000;
    config.keyboard = true;
    config.pauseOnHover = true;
   }

  ngOnInit(): void {

  }

}
