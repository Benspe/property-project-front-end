import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { PropertyService } from 'src/app/services/property/property.service';


@Component({
  selector: 'app-property',
  templateUrl: './property.component.html',
  styleUrls: ['./property.component.css']
})
export class PropertyComponent implements OnInit {

  property:any;
  loaded:boolean = false;
  images: string[];
  isGuest:boolean = true;
  isBuyer:boolean = false;
  isAgent:boolean = false;
  constructor(    
    private route: ActivatedRoute,
    private propertyService: PropertyService
    ) { }

  ngOnInit(): void {
    let givenPropertyId:Number = +this.route.snapshot.paramMap.get('propertyId');
    this.loadProperty(givenPropertyId);
    
  }

  async loadProperty(id:Number){
    try
    {
      this.property = await this.propertyService.getPropertyDetail(id).then(p => p.data);
      this.images = this.property.propertyImages;
      this.loaded = true;
      this.determineAccount()
    }
    catch(e)
    {
      console.error(e)
    }
  }

  determineAccount(){
    switch(this.property.accountType){
      case "Agent":
      this.isAgent = true;
      this.isGuest = false;
      break;
      case "Buyer":
        this.isAgent =true;
        this.isGuest = false;
        break;
      default:
        this.isGuest = true;
    }
  }
}
