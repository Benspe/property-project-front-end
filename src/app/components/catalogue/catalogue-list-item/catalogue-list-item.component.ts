import { Component,EventEmitter, Output, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-catalogue-list-item',
  templateUrl: './catalogue-list-item.component.html',
  styleUrls: ['./catalogue-list-item.component.css']
})
export class CatalogueListItemComponent implements OnInit {

  @Input() property;
  @Output() propertyClicked = new EventEmitter<any>();
  loaded = false;
  name:String;
  municipality: String;
  line: String;
  line2: String

  constructor() { }

  ngOnInit(): void {
    this.name = this.property.name;
    this.municipality = this.property.municipality;
    this.line = this.property.line;
    this.line2 = this.property.line2;
    this.loaded = true;
  }

  cardClicked(){
    this.propertyClicked.emit(this.property);
  }

}
