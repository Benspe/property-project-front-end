import { ComponentFixture, TestBed } from '@angular/core/testing';
import  { BrowserModule } from '@angular/platform-browser'; 
import { CatalogueListComponent } from './catalogue-list.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';

@NgModule({
  imports: [BrowserModule, NgbModule],
  declarations: [CatalogueListComponent],
  exports: [CatalogueListComponent],
  bootstrap: [CatalogueListComponent]
})
export class NgbdPaginationBasicModule {}

describe('CatalogueListComponent', () => {
  let component: CatalogueListComponent;
  let fixture: ComponentFixture<CatalogueListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalogueListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});

