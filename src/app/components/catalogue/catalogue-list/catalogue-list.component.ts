import { Component, NgModule, OnInit } from '@angular/core';
import { CatalogueService } from 'src/app/services/catalogue/catalogue.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  styleUrls: ['./catalogue-list.component.css']
})
export class CatalogueListComponent implements OnInit {

  loaded: Boolean=false
  page: Number=1
  properties: []
  constructor(
    private catalogueService : CatalogueService,
    private router: Router
    ) { }

  ngOnInit(): void {
    this.loadPropertiesFromAPI()
  }
  
  async loadNextPage(){
    this.loaded = false;
    this.loadPropertiesFromAPI();
    
  }
  
  async loadPropertiesFromAPI(){
    try
    {
      this.properties = await this.catalogueService.getAllProperties(this.page)
      this.loaded=true;
    }catch(e)
    {
      console.error(e.error);
    }
  }

  propertyClicked(property){
    this.router.navigate([`/property/${property.id}`]);
  }


}
