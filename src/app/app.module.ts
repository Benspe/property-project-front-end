import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CatalogueComponent } from './components/catalogue/catalogue.component';
import { PropertyComponent } from './components/property/property.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';
import { initializeKeycloak } from './AppInit';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { CatalogueListComponent } from './components/catalogue/catalogue-list/catalogue-list.component';
import { CatalogueListItemComponent } from './components/catalogue/catalogue-list-item/catalogue-list-item.component';
import { CarouselComponent } from './components/property/carousel/carousel.component';




@NgModule({
  declarations: [
    AppComponent,
    CatalogueComponent,
    PropertyComponent,
    HeaderComponent,
    HomeComponent,
    ProfileComponent,
    CatalogueListComponent,
    CatalogueListItemComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    KeycloakAngularModule,
    NgxPaginationModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
