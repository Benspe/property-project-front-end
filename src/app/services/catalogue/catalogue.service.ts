import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment'


@Injectable({
  providedIn: 'root'
})
export class CatalogueService {

  constructor(
    private http: HttpClient,
  ) { }
  
  getAllProperties(pageNumber):Promise<any>
  {
    return this.http.get(`${environment.apiUrl}property?pageNumber=${pageNumber}&pageSize=10`).toPromise()
    .then(
      (result: {data})=>{
        return result.data;
      }
    );
    
  }
  ngOnInit(){
    console.log("ngOnInit");
    console.log(this.getAllProperties(1));
    this.getAllProperties(1);
  }
}
