import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment'

@Injectable({
  providedIn: 'root'
})
export class PropertyService {

  constructor(
    private http: HttpClient,
  ) { }
  
  getPropertyDetail(id:Number):Promise<any>
  {
    return this.http.get(`${environment.apiUrl}property/${id}`).toPromise();
  }
}
