import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountRepositoryService {

  constructor(private http: HttpClient, private keycloak: KeycloakService) { }

  getAccountInfo(): Promise<any> {
    return this.http.get(environment.apiUrl + 'account').toPromise();
  }
  
  createAccount(): Promise<any> {
    return this.http.post(environment.apiUrl + 'account', "").toPromise();
  }
}
