import { KeycloakService } from 'keycloak-angular';
import { environment } from '../environments/environment';
//import { environmentProd } from '../environments/environment.prod';
 
export function initializeKeycloak(keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
      return new Promise(async (resolve, reject) => {
        try {
          await keycloak.init({
              config: {
                url: environment.keycloak.issuer,
                realm: environment.keycloak.realm,
                clientId: environment.keycloak.clientId,
              },
            loadUserProfileAtStartUp: true,
            initOptions: {
             onLoad: 'check-sso',
            silentCheckSsoRedirectUri:

              window.location.origin + environment.webAdress + '/assets/silent-check-sso.html'

           },
            bearerExcludedUrls: []
          });
          resolve();
        } catch (error) {
          console.log(error);
          reject(error);
        }
      });
   };
}