
export const environment = {
  production: false,
  keycloak: {
    issuer: 'https://propertiesauthentication.azurewebsites.net/auth',
    clientId: 'propertiesDevClient',
    realm: 'propertiesAuth'
  },

  webAdress: '',
  apiUrl: 'https://localhost:5001/api/'

};

