export const environment = {
  production: true,
  keycloak: {
    issuer: 'https://propertiesauthentication.azurewebsites.net/auth',
    clientId: 'productionClient',
    realm: 'propertiesAuth'
  },
  webAdress: '/property-project-front-end/',
  apiUrl:'https://e-properties.azurewebsites.net/api/'
};
